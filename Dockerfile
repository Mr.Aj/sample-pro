FROM maven:3.8-openjdk-11-slim As build
WORKDIR /app
COPY . .
RUN mvn clean install

FROM openjdk:11-jdk-slim-stretch
COPY --from=build  /app/target/*.jar app.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "app.jar"]